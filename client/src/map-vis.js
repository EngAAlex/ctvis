import React, { Component } from 'react';
import ReactMapGL, {NavigationControl} from 'react-map-gl';
import { CtLayers, CtLayerCfg, TimelineSlider} from './ct-layers';
import DeckGL, {MapController} from 'deck.gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import { withStyles } from "@material-ui/core/styles";

import smtModel from './25-370M-1sec.json';

const TOKEN = 'pk.eyJ1IjoidWJlcmRhdGEiLCJhIjoiY2pudzRtaWloMDAzcTN2bzN1aXdxZHB5bSJ9.2bkj3IiRC8wj3jLThvDGdA';

const MAP_VIS_STYLE = {
    hoverLabel: {
        position: 'absolute',
        padding: '4px',
        background: 'rgba(0, 0, 0, 0.7)',
        color: '#fff',
        maxWidth: '300px',
        fontSize: '12px',
        zIndex: 9,
        pointerEvents: 'none',
    },
    loader: {
        position: 'absolute',
        left: '50%',
        top: '50%',
        zIndex: 10,
        width: '150px',
        height: '150px',
        margin: '-75px 0 0 -75px',
        border: '16px solid #f3f3f3',
        borderRadius: '50%',
        borderTop: '16px solid #3498db',
        animation: 'spinAnimation 2s linear infinite',
    },
    '@keyframes spinAnimation': {
        '0%': {transform: 'rotate(0deg)'},
        '100%': {transform: 'rotate(360deg)'},
    }

};

const MAPVIS_INIT = {
    view: 'localView',
};


class MapVis extends Component {
    constructor(props) {
        super(props);
        const MIN_ZOOM = 6, MAX_ZOOM = 18, ZOOM = 7;
        let radius = this._calcHexagonRadius(ZOOM, MIN_ZOOM, MAX_ZOOM);
        this.state = {
            hover: {
                x: 0,
                y: 0,
                hoveredObject: null
            },
            regionalBins:[],
            localFirms: [],
            localFirmIndicesObj: {},
            localSelectedFirmsBySector: [],
            firmsYearlys:[],
            euRatiosYearlys:[],
            euRatioYearlySumMax: {
                cashFlow: 1,
            },
            smtCashFlows:{},
            layerCfg: {
                view: MAPVIS_INIT.view,
                hexagonRadius: radius,
                hexagonVisible: true,
                localArcVisible: true,
                timelineOption: 'firmDensity',
                regionLevel: 'bundesland',
                regionMinCount: 1,
                regionMaxCount: 1,
                regionRadius: 500,
                selectedSectors: new Set(),
                timelineEnabled: true,
                currDate: 2007.25,
            },
            viewState: {
                longitude: 13.2420508,
                latitude: 47.567685,    
                zoom: 7,
                minZoom: MIN_ZOOM,
                maxZoom: MAX_ZOOM,
                pitch: 60,
                bearing: 0
            },
            oenaceLevelOne: {},
            initFinished: false,
            mapStyle: 'mapbox://styles/mapbox/dark-v9'
            
        };
    }

    async componentDidMount() {
        console.log("loading data...");
        if (MAPVIS_INIT.view === 'localView') {
            let localState = await this._loadLocalViewData(this.state.layerCfg);
            this.setState({...localState, initFinished: true});
        } else if (MAPVIS_INIT.view === 'regionalView') {
            let regionState = await this._loadRegionalViewData(this.state.layerCfg);
            this.setState({...regionState, initFinished: true});
        }
    }

    _parseSmtModel = (smtModel, localFirms, localFirmIndicesObj) => {
        let firm_smt_indices = Object.keys(smtModel);
        let firm_smt_positions = [];
        firm_smt_indices.forEach(model_id => {
            let firm_id = smtModel[model_id].sabina_id;
            let index_in_local_firms = localFirmIndicesObj[firm_id.toString()];
            if (index_in_local_firms === undefined) {
                console.log("cannot find firm with id = " + firm_id);
                let firm_pos = [12.335833, 45.4375]; // Venice
                firm_smt_positions.push(firm_pos);
            } else {
                let firm_pos = localFirms[index_in_local_firms].position;
                firm_smt_positions.push(firm_pos);
            }
        });

        let smtCashFlows = [];
        let minFlowAmount = Number.MAX_VALUE;
        let maxFlowAmount = 0;
        firm_smt_indices.forEach((model_id) => {
            smtModel[model_id].outflows.forEach((flow, dest_id) => {
                const smt_cash_flow = {
                    from: {
                        firmId: smtModel[model_id].sabina_id,
                        position: [...firm_smt_positions[Number(model_id)]],
                    },
                    to: {
                        firmId: smtModel[dest_id].sabina_id,
                        position: [...firm_smt_positions[Number(dest_id)]],
                    },
                    amount: flow,
                };
                smtCashFlows.push(smt_cash_flow);
                minFlowAmount = minFlowAmount < flow ? minFlowAmount : flow;
                maxFlowAmount = maxFlowAmount > flow ? maxFlowAmount : flow;
            });
        });
        return {
            flowVecs: smtCashFlows,
            domain: [minFlowAmount, maxFlowAmount]
        };
    };

    _loadLocalViewData = async (layerCfg) => {
        console.log("loading local data...");
        const localFirms = [];
        const localFirmIndicesObj = {};

        const parseLocalData = ((row, idx) => {
            let lat = Number(row.latitude);
            let lng = Number(row.longitude);
            let firm_id = Number(row.firm_id);
            let edv_e1 = row.edv_e1;
            localFirms.push({position:[lng, lat], firm_id:firm_id, edv_e1: edv_e1});
            localFirmIndicesObj[firm_id.toString()] = idx;
        });
        
        try {
            const localData = await this._retrieveLocalData(layerCfg);
            localData.forEach((row, idx) => {parseLocalData(row, idx);});
            layerCfg.regionMaxCount = 1;
            layerCfg.regionMinCount = 1;
            layerCfg.selectedSectors.clear();
            let oenaceLevelOne = {};
            let euRatiosYearlys = [];
            let firmsYearlys = [];

            const oenace = await this._retriveOenaceData();
            oenace.forEach(sec => {
                layerCfg.selectedSectors.add(sec.edv_e1);
                oenaceLevelOne[sec.edv_e1] = sec.desc;
            });

            let eu_ratios_data = await this._retrieveEuRatiosData();
            let eu_ratios = eu_ratios_data.ratios;
            let eu_ratios_intervals = eu_ratios_data.intervals;

            let eu_ratio_yearly_sum_max = {cashFlow: 1};
            for (let i = 0; i < eu_ratios_intervals.length - 1; ++i) {
                let beg = eu_ratios_intervals[i];
                let end = eu_ratios_intervals[i + 1];
                let firm_id_set = new Set();
                let eu_ratio_obj = {};
                let cash_flow_yearly_sum = 0;
                eu_ratios.slice(beg, end).forEach( firm => {
                    firm_id_set.add(firm.firm_id);
                    eu_ratio_obj[firm.firm_id.toString()] = {cashFlow: firm.cash_flow};
                    cash_flow_yearly_sum += Math.abs(firm.cash_flow);
                });
                eu_ratio_yearly_sum_max.cashFlow = Math.max(cash_flow_yearly_sum, eu_ratio_yearly_sum_max.cashFlow);
                firmsYearlys.push(firm_id_set);
                euRatiosYearlys.push(eu_ratio_obj);
            }
            let smtCashFlows = this._parseSmtModel(smtModel, localFirms, localFirmIndicesObj);
            // console.log("smtCashFlows: \n" + JSON.stringify(smtCashFlows));

            let localSelectedFirmsBySector = [...localFirms];
            return {
                localFirms: localFirms,
                localFirmIndicesObj: localFirmIndicesObj,
                localSelectedFirmsBySector: localSelectedFirmsBySector, 
                firmsYearlys: firmsYearlys,
                euRatiosYearlys: euRatiosYearlys,
                smtCashFlows: smtCashFlows,
                layerCfg: layerCfg, 
                oenaceLevelOne: oenaceLevelOne,
                euRatioYearlySumMax: eu_ratio_yearly_sum_max,
            };

        } catch(err) {
            console.log(err);
        };
    };

    _loadRegionalViewData = async (layerCfg) => {
        console.log("loading regional data...");
        const regionalBins = [];
        const parseRegionalData = (row => {
            let lat = Number(row.latitude);
            let lng = Number(row.longitude);
            let firmCount = Number(row.count);
            let bundesland = <div>{row.bundesland}</div>;
            if (layerCfg.regionLevel === 'bundesland') {
                layerCfg.regionRadius = 12000;
                regionalBins.push({position:[lng, lat], elevation: firmCount, label: bundesland});
            } else {
                let bezirk = <div><div>{row.bezirk}</div>{bundesland}</div>;
                if (layerCfg.regionLevel === 'bezirk') {
                    layerCfg.regionRadius = 2500;
                    regionalBins.push({position:[lng, lat], elevation: firmCount, label: bezirk});
                } else {
                    let gemeinde = <div><div>{row.gemeinde}</div>{bezirk}</div>;
                    if (layerCfg.regionLevel === 'gemeinde') {
                        layerCfg.regionRadius = 1000;
                        regionalBins.push({position:[lng, lat], elevation: firmCount, label: gemeinde});
                    } else if (layerCfg.regionLevel === 'ort') {
                        layerCfg.regionRadius = 300;
                        let ort = <div><div>{row.ort}</div>{gemeinde}</div>;
                        regionalBins.push({position:[lng, lat], elevation: firmCount, label: ort});
                    }
                }
            }
        });

        try {
            const body = await this._retrieveRegionalData(layerCfg);
            body.forEach(row => {parseRegionalData(row);});
            // ensured by sql query: order by count desc
            layerCfg.regionMaxCount = body[0].count;
            layerCfg.regionMinCount = Math.ceil(body[body.length - 1].count / 1.25);
            let state = {
                regionalBins: regionalBins, 
                layerCfg: layerCfg, 
            };
            return state;
        } catch(err) {
            console.log(err);
            return {};
        };
    };


    _retriveOenaceData = async () => {
        try {
            const response = await fetch('/oenace_data',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({})
            });
            const body = await response.json();
            if (response.status !== 200) throw Error(body.message);
            return body;
        } catch (err) {
            console.log(err.message, err.stack);
            return [];
        }
    }

    _retrieveRegionalData = async (layerCfg) =>  {
        try {
            const response = await fetch('/regional_data',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({regionLevel: layerCfg.regionLevel })
            });
            const regionalData = await response.json();
            if (response.status !== 200) throw Error(regionalData.message);
            return regionalData;
        } catch (err) {
            console.log(err.message, err.stack);
            return [];
        }
    };

    _retrieveLocalData = async (layerCfg) =>  {
        try {
            const response = await fetch('/local_data',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({view: layerCfg.view})
            });
            const localData = await response.json();
            if (response.status !== 200) throw Error(localData.message);
            return localData;
        } catch (err) {
            console.log(err.message, err.stack);
            return [];
        }
    };

    _retrieveEuRatiosData = async () =>  {
        try {
            const response = await fetch('/eu_ratios_data',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({})
            });
            const eu_ratios_data = await response.json();
            if (response.status !== 200) throw Error(eu_ratios_data.message);
            return eu_ratios_data;
        } catch (err) {
            console.log(err.message, err.stack);
            return {};
        }
    };


    // not in use
    _retrieveGeoSecData = async (layerCfg) =>  {
        try {
            const response = await fetch('/geo_sec_data',{
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({view: layerCfg.view, regionLevel: layerCfg.regionLevel })
            });
            const body = await response.json();
            if (response.status !== 200) throw Error(body.message);
            return body;
        } catch (err) {
            console.log(err.message, err.stack);
            return [];
        }
    };

    _onHover = ({ x, y, object, layer}) => {
        let label = null;
        let layerCfg = this.state.layerCfg;
        if (object) {
            if (layerCfg.view === 'localView' ) {
                if (layer.id === 'local-hexagon') {
                    if (!layerCfg.timelineEnabled || layerCfg.timelineOption === 'firmDensity' || layerCfg.timelineOption === 'firmDensityGrad') {
                        let label_text = object.elevationValue * Math.sign(object.colorValue) + (object.elevationValue > 1 ? ' firms' : ' firm');
                        label = <div>{label_text}</div>;
                    }  else if (layerCfg.timelineOption === 'cashFlow' || layerCfg.timelineOption === 'cashFlowGrad') {
                        let value = object.elevationValue;
                        let suffix = "";
                        if (value >= 10**12) {
                            value = (value /(10**12)).toFixed(3);
                            suffix = " Tn";
                        } else if (value >= 10**9) {
                            value = (value / (10**9)).toFixed(3);
                            suffix = " Bn";
                        } else if (value >= 10**6) {
                            value = (value / (10 ** 6)).toFixed(3);
                            suffix = " M";
                        } else if (value >= 10**3) {
                            value = (value / (10 ** 3)).toFixed(3);
                            suffix = " K";
                        }
                        value *= Math.sign(object.colorValue);
                        label = <div>{value}{suffix}</div>;
                    } else {
                        label = null;
                    }
                } else if (layer.id === 'local-cash-flow-arch') {
                    label = <div>from {object.from.firmId} to {object.to.firmId}: {object.amount}</div>;

                } else {
                    label = null;
                }
            } else if (layerCfg.view === 'regionalView') {
                let firm_text = (object.elevation > 1 ? ' firms' : ' firm') ;
                label = <div>{object.label}{object.elevation} {firm_text}</div>;
            }
        }
        else {
            label = null;
        }
        this.setState({ hover: { x, y, hoveredObject: object, label } });
    };

    _updateLayerCfg = (layerCfg) => {
        this.setState({ layerCfg: layerCfg });
    };
    
    _onSelectedSectorsChange = (layerCfg) => {
        let localSelectedFirmsBySector = this.state.localFirms.filter(
            firm => {
                return layerCfg.selectedSectors.has(firm.edv_e1);
            }
        );
        this.setState({layerCfg: layerCfg, localSelectedFirmsBySector: localSelectedFirmsBySector});
    };
    _onMapStyleChange = (event) => {
        this.setState({mapStyle: event.target.value});
    }

    _onViewOptionChange = async (event) => {
        const currView = event.target.value;
        if (currView === this.state.layerCfg.view) {
            return;
        }
        const currLayerCfg = {
            ...this.state.layerCfg,
            view: currView
        };
        this.setState({initFinished: false}, async () => {
        if (currView === 'localView') {
            let localState = await this._loadLocalViewData(currLayerCfg);
            this.setState({...localState, initFinished: true});
            
        } else if (currView === 'regionalView') {
            let regionState = await this._loadRegionalViewData(currLayerCfg);
            this.setState({...regionState, initFinished: true});
        }});
        
    }

    _onRegionLevelChange = async (event) => {
        const currRegionLevel = event.target.value;
        if (currRegionLevel === this.state.layerCfg.regionLevel) {
            return;
        }
        const currLayerCfg = {
            ...this.state.layerCfg,
            regionLevel: currRegionLevel
        };
        this.setState({initFinished: false}, async () => {
            let regionState = await this._loadRegionalViewData(currLayerCfg);
            this.setState({...regionState, initFinished: true});
        });
    }

    _calcHexagonRadius = (zoom, minZoom, maxZoom) => {
        const minHexRadius = 25, maxhexRadius = 6000;
        let scaleChange = (maxZoom - zoom) / (maxZoom - minZoom);
        let hexRadius = Math.round(scaleChange **3 * (maxhexRadius - minHexRadius)  + minHexRadius);
        return (hexRadius - hexRadius % minHexRadius);
    }

    _onViewStateChange = ({viewState}) => {
        const {layerCfg} = this.state;
        if (viewState.zoom < viewState.minZoom || viewState.zoom > viewState.maxZoom) {
            return;
        }
        let zoom = Math.round(viewState.zoom);
        if (this.state.viewState.zoom !== viewState.zoom) {
            let radius = this._calcHexagonRadius(zoom, viewState.minZoom, viewState.maxZoom);
            if (radius !== layerCfg.hexagonRadius) {
                const currLayerCfg = {
                    ...layerCfg,
                    hexagonRadius: radius
                };
                this.setState({viewState: viewState, layerCfg: currLayerCfg});
            } else {
                this.setState({viewState: viewState});
            }
        }
        else {
            this.setState({viewState: viewState});
        }
    }

    render() {
        const { classes } = this.props;
        const {viewState, hover, initFinished} = this.state;
        let loader = (<div></div>);
        if (!initFinished) {
            loader = (<div className={classes.loader}></div>);
        }
        return (
            <div onContextMenu={event => event.preventDefault()}>
                {hover.hoveredObject && (
                    <div
                        className={classes.hoverLabel}
                        style={{transform: `translate(${hover.x}px, ${hover.y}px)`}}
                    >
                        {hover.label}
                    </div>
                )}
                {loader}
                <CtLayerCfg
                    layerCfg={this.state.layerCfg}
                    mapStyle={this.state.mapStyle}
                    oenaceLevelOne={this.state.oenaceLevelOne}
                    onSelectedSectorsChange={layerCfg => this._onSelectedSectorsChange(layerCfg)}
                    onLayerCfgChange={layerCfg => this._updateLayerCfg(layerCfg)}
                    onMapStyleChange={event => this._onMapStyleChange(event)}
                    onViewOptionChange={event => this._onViewOptionChange(event)}
                    onRegionLevelChange={event => this._onRegionLevelChange(event)}
                />
                <DeckGL
                    onWebGLInitialized={this._onWebGLInitialize}
                    layers={[ new CtLayers({
                        regionalBins: this.state.regionalBins,
                        localSelectedFirmsBySector: this.state.localSelectedFirmsBySector,
                        localFirms: this.state.localFirms,
                        firmsYearlys: this.state.firmsYearlys,
                        euRatiosYearlys: this.state.euRatiosYearlys,
                        euRatioYearlySumMax: this.state.euRatioYearlySumMax,
                        smtCashFlows: this.state.smtCashFlows,
                        onHover: hover => this._onHover(hover),
                        layerCfg: this.state.layerCfg,
                        viewState:this.state.viewState,
                    })]}
                    viewState={viewState}
                    onViewStateChange={({viewState}) => this._onViewStateChange({viewState})}
                    controller={{type:MapController}}
                >
                    <ReactMapGL 
                        mapboxApiAccessToken={TOKEN} 
                        mapStyle={this.state.mapStyle}
                    >
                        <div style={{position: 'absolute', left: '13px', top: '13px'}}>
                            <NavigationControl showZoom={false} />
                        </div>
                    </ReactMapGL>
                </DeckGL>
                <TimelineSlider 
                    layerCfg={this.state.layerCfg} 
                    onLayerCfgChange={layerCfg => this._updateLayerCfg(layerCfg)}
                />
                
        </div>
        );
    }  
}


export default withStyles(MAP_VIS_STYLE)(MapVis);