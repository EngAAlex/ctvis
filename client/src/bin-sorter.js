const defaultGetValue = points => points.length;

export default class BinSorter {
  constructor(bins = [], getValue = defaultGetValue) {
    this.sortedBins = this.getSortedBins(bins, getValue);
    this.maxCount = this.getMaxCount();
    this.binMap = this.getBinMap();
  }

  /**
   * Get an array of object with sorted values and index of bins
   * @param {Array} bins
   * @param {Function} getValue
   * @return {Array} array of values and index lookup
   */
  getSortedBins(bins, getValue) {
    return bins
      .reduce((accu, h, i) => {
        const value = getValue(h);

        if (value !== null && value !== undefined) {
          // filter bins if value is null or undefined
          accu.push({
            i: Number.isFinite(h.index) ? h.index : i,
            value,
            counts: h.points.length
          });
        }

        return accu;
      }, [])
      .sort((a, b) => Math.abs(a.value) - Math.abs(b.value));
  }

  /**
   * Get range of values of all bins
   * @param {Number[]} range
   * @param {Number} range[0] - lower bound
   * @param {Number} range[1] - upper bound
   * @return {Array} array of new value range
   */
  getValueRange([lower, upper]) {
    const len = this.sortedBins.length;
    if (!len) {
      return [0, 0];
    }
    const lowerIdx = Math.ceil((lower / 100) * (len - 1));
    const upperIdx = Math.floor((upper / 100) * (len - 1));

    return [Math.abs(this.sortedBins[lowerIdx].value), Math.abs(this.sortedBins[upperIdx].value)];
  }

  /**
   * Get ths max count of all bins
   * @return {Number | Boolean} max count
   */
  getMaxCount() {
    let maxCount = 0;
    this.sortedBins.forEach(x => (maxCount = maxCount > x.counts ? maxCount : x.counts));
    return maxCount;
  }

  /**
   * Get a mapping from cell/hexagon index to sorted bin
   * This is used to retrieve bin value for color calculation
   * @return {Object} bin index to sortedBins
   */
  getBinMap() {
    return this.sortedBins.reduce(
      (mapper, curr) =>
        Object.assign(mapper, {
          [curr.i]: curr
        }),
      {}
    );
  }
}